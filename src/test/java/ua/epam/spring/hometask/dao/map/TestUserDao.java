package ua.epam.spring.hometask.dao.map;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.User;

public class TestUserDao {
	
	private UserDao dao;

	@Before
	public void setup() {
		dao = new UserDaoMapImpl();
		
		User user = createUser("111", "222", "333@333.com");
		dao.save(user);
		
		user = createUser("444", "444", "555@555.com");
		dao.save(user);
	}

	@Test
	public void testSaveUser() {
		User user = createUser("aaa", "bbb", "ccc@ccc.com");
		
		User savedUser = dao.save(user);
		
		assertNotNull(savedUser.getId());
		
		assertEquals(savedUser, user);
	}
	
	@Test
	public void testGetUser() {
		User user = createUser("aaa", "bbb", "ccc@ccc.com");
		User savedUser = dao.save(user);
		
		assertNotNull(savedUser.getId());
		
		Integer id = savedUser.getId();
		
		Collection<User> users = dao.getAll();
		assertFalse(users.isEmpty());
		assertTrue(users.contains(savedUser));
		
		User byId = dao.getById(id);
		assertNotNull(byId);
		assertEquals(savedUser, byId);
		
		byId = dao.getById(-1);
		assertNull(byId);
		
		User byEmail = dao.getByEmail(user.getEmail());
		assertNotNull(byEmail);
		assertEquals(savedUser, byEmail);
		
		byEmail = dao.getByEmail("zzz");
		assertNull(byEmail);
	}
	
	@Test
	public void testRemoveUser() {
		User user = createUser("aaa", "bbb", "ccc@ccc.com");
		User savedUser = dao.save(user);
		
		dao.remove(savedUser.getId());
		
		Collection<User> users = dao.getAll();
		assertFalse(users.isEmpty());
		assertFalse(users.contains(savedUser));
	}

	private User createUser(String firstName, String lastName, String email) {
		User user = new User();
		user.setEmail(email);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		return user;
	}

}
