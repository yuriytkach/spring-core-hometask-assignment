package ua.epam.spring.hometask.domain;

import java.util.Arrays;
import java.util.Properties;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestAuditorium {
	
	@Test (expected=IllegalArgumentException.class)
	public void testCreationNoName() {
		Properties props = new Properties();
		props.put("seats", "10");
		new Auditorium(props);
		
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testCreationNoSeats() {
		Properties props = new Properties();
		props.put("name", "aaa");
		new Auditorium(props);
	}
	
	@Test
	public void testCreationNoVips() {
		Properties props = new Properties();
		props.put("name", "aaa");
		props.put("seats", "10");
		
		Auditorium a = new Auditorium(props);
		
		assertEquals("aaa", a.getName());
		assertEquals(10, a.getNumberOfSeats());
		assertNotNull(a.getVipSeats());
		assertTrue(a.getVipSeats().isEmpty());
	}
	
	@Test
	public void testCreationVips() {
		Properties props = new Properties();
		props.put("name", "aaa");
		props.put("seats", "10");
		props.put("vip", "1,2,3");
		
		Auditorium a = new Auditorium(props);
		
		assertEquals("aaa", a.getName());
		assertEquals(10, a.getNumberOfSeats());
		assertNotNull(a.getVipSeats());
		assertEquals(3, a.getVipSeats().size());
		assertTrue(a.getVipSeats().contains(1L));
		assertTrue(a.getVipSeats().contains(2L));
		assertTrue(a.getVipSeats().contains(3L));
	}
	
	@Test
	public void testCountVips() {
		Properties props = new Properties();
		props.put("name", "aaa");
		props.put("seats", "10");
		props.put("vip", "1,2,3");
		
		Auditorium a = new Auditorium(props);
		
		assertEquals(0, a.countVipSeats(Arrays.asList(10L, 20L, 30L)));
		assertEquals(1, a.countVipSeats(Arrays.asList(10L, 2L, 30L)));
		assertEquals(2, a.countVipSeats(Arrays.asList(10L, 2L, 3L, 4L, 5L, 6L)));
	}

}
