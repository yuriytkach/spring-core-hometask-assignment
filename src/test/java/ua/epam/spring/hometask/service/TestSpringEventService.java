package ua.epam.spring.hometask.service;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;

@ContextConfiguration("classpath:spring.xml")
public class TestSpringEventService extends AbstractJUnit4SpringContextTests {
	
	@Test
	public void testUserServiceIsDefinedAsBean() {
		boolean contains = applicationContext.containsBean("eventService");
		assertTrue(contains);
	}

	@DirtiesContext
	@Test
	public void testSavingEvent() {
		EventService service = applicationContext.getBean(EventService.class);
		assertNotNull(service);
		
		Event event = createEvent();
		
		Event registered = service.save(event);
		assertNotNull(registered);
		assertNotNull(registered.getId());
		assertEquals(event, registered);
	}
	
	@Test
	@DirtiesContext
	public void testGetEvent() {
		EventService service = applicationContext.getBean(EventService.class);
		Event event = createEvent();
		
		Event registered = service.save(event);
		
		Event byId = service.getById(registered.getId());
		assertEquals(registered, byId);
	}
	
	@Test
	@DirtiesContext
	public void testDeleteEvent() {
		EventService service = applicationContext.getBean(EventService.class);
		Event event = createEvent();
		
		Event registered = service.save(event);
		service.remove(registered);
		
		Event byId = service.getById(registered.getId());
		assertNull(byId);
	}
	
	@Test
	@DirtiesContext
	public void testAssignAuditoriums() {
		EventService service = applicationContext.getBean(EventService.class);
		Event event = createEvent();
		Event registered = service.save(event);
		
		AuditoriumService auditoriumService = applicationContext.getBean(AuditoriumService.class);
		Auditorium auditorium = auditoriumService.getAll().iterator().next();
		
		boolean result = service.assignAuditoriumToEvent(registered, event.getAirDates().first(), auditorium);
		assertTrue(result);
		
	}

	private Event createEvent() {
		Event event = new Event();
		event.setName("aaa");
		event.setRating(EventRating.HIGH);
		event.setBasePrice(100.10);
		event.addAirDateTime(LocalDateTime.now());
		return event;
	}

}
