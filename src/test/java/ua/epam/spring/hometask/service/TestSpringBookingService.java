package ua.epam.spring.hometask.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.User;

@ContextConfiguration("classpath:spring.xml")
public class TestSpringBookingService extends AbstractJUnit4SpringContextTests {
	
	@Test
	public void testAuditoriumServiceIsDefinedAsBean() {
		boolean contains = applicationContext.containsBean("bookingService");
		assertTrue(contains);
	}
	
	@Test
	public void testGetTicketPrice() {
		AuditoriumService auditoriumService = applicationContext.getBean(AuditoriumService.class);
		Set<Auditorium> auditoriums = auditoriumService.getAll();
		Auditorium auditorium = auditoriums.iterator().next();
		auditorium.setNumberOfSeats(10);
		auditorium.setVipSeats(new HashSet<Long>(Arrays.asList(1L, 2L, 3L)));
		
		Event event = createEvent();
		User user = createUser();
		
		UserService userService = applicationContext.getBean(UserService.class);
		user = userService.save(user);
		
		EventService eventService = applicationContext.getBean(EventService.class);
		event = eventService.save(event);
		LocalDateTime time = event.getAirDates().first();
		event.assignAuditorium(time, auditorium);
		
		BookingService bookingService = applicationContext.getBean(BookingService.class);
		bookingService.setVipSeatMultiplier(1.5);
		bookingService.setHighRatingMultiplier(1.1);
		
		double price = bookingService.getTicketPrice(event, time, Arrays.asList(1L,5L), user);
		assertEquals(100*1.1 + 100*1.1*1.5, price, 0.001);
	}
	
	private Event createEvent() {
		Event event = new Event();
		event.setName("aaa");
		event.setRating(EventRating.HIGH);
		event.setBasePrice(100);
		event.addAirDateTime(LocalDateTime.now());
		return event;
	}
	
	private User createUser() {
		User user = new User();
		user.setEmail("email@email.com");
		user.setFirstName("aaa");
		user.setLastName("bbb");
		return user;
	}

}
