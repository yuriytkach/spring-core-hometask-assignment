package ua.epam.spring.hometask.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Auditorium {

	private String name;

	private long numberOfSeats;

	private Set<Long> vipSeats;

	public Auditorium() {
	}

	public Auditorium(Properties props) {
		name = props.getProperty("name");
		numberOfSeats = Integer.valueOf(props.getProperty("seats", "0"));
		String vips = props.getProperty("vip", null);
		if (vips != null) {
			vipSeats = Stream.of(vips.split(",")).map(Long::parseLong)
					.collect(Collectors.toSet());
		} else {
			vipSeats = Collections.emptySet();
		}
		if (name == null || numberOfSeats == 0) {
			throw new IllegalArgumentException(
					"Auditorium name or seats number is not passed. Expected properties 'name' and 'seats'.");
		}
	}

	/**
	 * Counts how many vip seats are there in supplied <code>seats</code>
	 * 
	 * @param seats
	 *            Seats to process
	 * @return number of vip seats in request
	 */
	public long countVipSeats(Collection<Long> seats) {
		return seats.stream().filter(seat -> vipSeats.contains(seat)).count();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(long numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public Set<Long> getVipSeats() {
		return vipSeats;
	}

	public void setVipSeats(Set<Long> vipSeats) {
		this.vipSeats = vipSeats;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Auditorium other = (Auditorium) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

}
