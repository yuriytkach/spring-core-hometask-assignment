package ua.epam.spring.hometask.dao.map;

import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Event;

public class EventDaoMapImpl extends AbstractDomainObjectDaoMapImpl<Event> implements EventDao {
	
	/* (non-Javadoc)
	 * @see ua.epam.spring.hometask.dao.map.EventDao#getByName(java.lang.String)
	 */
	@Override
	public Event getByName(String name) {
		return findFirst(e -> e.getValue().getName().equals(name));
	}
}
