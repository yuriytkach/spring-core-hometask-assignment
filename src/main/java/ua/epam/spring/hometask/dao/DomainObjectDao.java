package ua.epam.spring.hometask.dao;

import java.util.Collection;

import ua.epam.spring.hometask.domain.DomainObject;

public interface DomainObjectDao<T extends DomainObject> {
	
	public abstract T save(T object);

	public abstract void remove(Integer id);

	public abstract Collection<T> getAll();

	public abstract T getById(Integer id);

}
