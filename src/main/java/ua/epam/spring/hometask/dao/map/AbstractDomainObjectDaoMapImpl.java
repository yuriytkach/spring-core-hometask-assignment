package ua.epam.spring.hometask.dao.map;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import ua.epam.spring.hometask.dao.DomainObjectDao;
import ua.epam.spring.hometask.domain.DomainObject;

public abstract class AbstractDomainObjectDaoMapImpl<T extends DomainObject> implements DomainObjectDao<T>{
	
	private final AtomicInteger SEQUENCE = new AtomicInteger(0);

	protected final Map<Integer, T> map = new ConcurrentHashMap<>();
	
	@Override
	public T save(T object) {
		if (object.getId() == null) {
			object.setId(SEQUENCE.incrementAndGet());
		}
		map.put(object.getId(), object);
		return object;
	}

	@Override
	public void remove(Integer id) {
		map.remove(id);
	}

	@Override
	public Collection<T> getAll() {
		return map.values();
	}

	@Override
	public T getById(Integer id) {
		return map.get(id);
	}
	
	protected T findFirst(Predicate<? super Entry<Integer, T>> predicate) {
		Optional<Entry<Integer, T>> opt = map.entrySet().stream()
				.filter(predicate).findFirst();

		if (opt.isPresent()) {
			return opt.get().getValue();
		} else {
			return null;
		}
	}

}
