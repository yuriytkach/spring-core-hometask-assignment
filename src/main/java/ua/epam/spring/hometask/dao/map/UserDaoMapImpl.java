package ua.epam.spring.hometask.dao.map;

import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.User;

public class UserDaoMapImpl extends AbstractDomainObjectDaoMapImpl<User> implements UserDao {

	/* (non-Javadoc)
	 * @see ua.epam.spring.hometask.dao.map.UserDao#getByEmail(java.lang.String)
	 */
	@Override
	public User getByEmail(String email) {
		return findFirst(e -> e.getValue().getEmail().equals(email));
	}

}
