package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.User;

public class UserService extends AbstractDomainObjectService<User, UserDao>{

	public UserService(UserDao dao) {
		super(dao);
	}

	public User getUserByEmail(String email) {
		return dao.getByEmail(email);
	}

}
