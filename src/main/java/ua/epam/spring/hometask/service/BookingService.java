package ua.epam.spring.hometask.service;

import java.time.LocalDateTime;
import java.util.List;

import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.User;

public class BookingService {
	
	private double vipSeatMultiplier = 1;
	
	private double highRatingMultiplier = 1;
	
	public double getTicketPrice(Event event, LocalDateTime dateTime, List<Long> seats, User user) {
		if (event.airsOnDateTime(dateTime)) {
			
			double priceBase = event.getBasePrice();
			
			if (EventRating.HIGH == event.getRating()) {
				priceBase *= highRatingMultiplier;
			}
			
			Auditorium auditorium = event.getAuditoriums().get(dateTime);
			
			long vipsCount = auditorium.countVipSeats(seats);
			long basicCount = seats.size() - vipsCount;
			
			double priceVipSeats = priceBase * vipSeatMultiplier * vipsCount;
			double priceBasicSeats = priceBase * basicCount;
			
			double price = priceVipSeats + priceBasicSeats;
			
			return price;
		}
		
		return -1;
	}

	public void setVipSeatMultiplier(double vipSeatMultiplier) {
		this.vipSeatMultiplier = vipSeatMultiplier;
	}

	public void setHighRatingMultiplier(double highRatingMultiplier) {
		this.highRatingMultiplier = highRatingMultiplier;
	}

}
