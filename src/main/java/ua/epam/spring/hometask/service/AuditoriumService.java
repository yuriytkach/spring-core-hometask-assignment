package ua.epam.spring.hometask.service;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import ua.epam.spring.hometask.domain.Auditorium;

public class AuditoriumService {
	
	private Set<Auditorium> auditoriums;

	public AuditoriumService(Set<Auditorium> auditorium) {
		this.auditoriums = Collections.unmodifiableSet(auditorium);
	}
	
	public Set<Auditorium> getAll() {
		return auditoriums;
	}
	
	public Auditorium getByName(String name) {
		Optional<Auditorium> opt = auditoriums.stream()
				.filter(a -> a.getName().equals(name)).findFirst();

		if (opt.isPresent()) {
			return opt.get();
		} else {
			return null;
		}
	}

}
