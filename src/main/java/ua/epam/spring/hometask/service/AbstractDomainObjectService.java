package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.dao.DomainObjectDao;
import ua.epam.spring.hometask.domain.DomainObject;

public abstract class AbstractDomainObjectService <T extends DomainObject, D extends DomainObjectDao<T>> {
	
	protected D dao;

	public AbstractDomainObjectService(D dao) {
		this.dao = dao;
	}
	
	public T save(T object) {
		return dao.save(object);
	}

	public void remove(T object) {
		dao.remove(object.getId());
	}

	public T getById(Integer id) {
		return dao.getById(id);
	}

}
