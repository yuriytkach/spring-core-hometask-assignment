package ua.epam.spring.hometask.service;

import java.time.LocalDateTime;

import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;

public class EventService extends AbstractDomainObjectService<Event, EventDao> {

	public EventService(EventDao dao) {
		super(dao);
	}

	public boolean assignAuditoriumToEvent(Event event, LocalDateTime dateTime,
			Auditorium auditorium) {
		boolean result = event.assignAuditorium(dateTime, auditorium);
		if (result) {
			dao.save(event);
		}
		return result;
	}

}
